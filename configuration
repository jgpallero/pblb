#!/bin/bash
# -*- coding: utf-8 -*-
# Written by José Luis García Pallero, jgpallero@gmail.com, September 2013
# Public domain

# This text will be used as header for figures
HARDWARE="Intel Core i5-2500 3.30GHz Quad-Core"

# Programs to use
# This vector must contain the names of the executables to use. If any program
# is linked against ATLAS, the final name of the executable is composed by the
# part stored in this vector and the number of threads stored in NTHREADS. See
# below
PROGRAMS=(testing-OB testing-ATL testing-MKL testing-SP)

# Use BLAS functions
# This vector must has the same length as PROGRAMS. Each element must indicate
# if the BLAS functions should be used in the corresponding program
BLAS=(1 1 1 1)

# Use Lapack functions
# This vector must has the same length as PROGRAMS. Each element must indicate
# if the Lapack functions should be used in the corresponding program
LAPACK=(0 1 1 1)

# Programs linked against ATLAS
# This vector must has the same length as PROGRAMS. Each element must indicate
# if the correspondent program uses ATLAS or not. The possible values are: 0, if
# the program does not use ATLAS or 1, if the program does use it
ATLAS=(0 1 0 0)

# Text for legend
# This vector must has the same length as PROGRAMS. The elements will be used in
# the plots' legend
LEGEND=("OpenBLAS 0.2.8" "ATLAS 3.11.13" "MKL 13.0" "SunPerf 12.3")

# Functions
# This vector must contain the functions to test. There are defined:
# From BLAS: (S|D)GEMM, (S|D)TRMM and (S|D)SYRK
# From Lapack: (S|D)GETRF and (S|D)GEQRF
FUNCTIONS=(SGEMM DGEMM STRMM DTRMM SSYRK DSYRK SGETRF DGETRF SGEQRF DGEQRF)

# Vector containing the number of threads to use
NTHREADS=(1 2 3 4)

# Available threads for ATLAS
# This vector must has the same length as NTHREADS. Each element indicates if
# the correspondent number of threads are available for ATLAS. The possible
# values are: 0, if the number of threads are not available for ATLAS or 1, if
# they are available. For ATLAS, the final name of the executable is composed by
# the basename stored in PROGRAMS and the elements of NTHREADS for which this
# vector (ATHREADS) stores the value 1, i.e. basename-1, basename-4, etc. The
# character '-' before the number of threads in the name is mandatory in the
# name of the program to execute
ATHREADS=(1 0 0 1)

# Hardware Rmax for each thread number defined in NTHREADS for singe and double
# precision, in GFLOPS/s. This vector must has the same length as NTHREADS. If
# the Rmax is unknown, the correspondent element must be 0 or 0.0
RMAXS=(52.8 105.6 158.4 211.2)
RMAXD=(26.4  52.8  79.2 105.6)

# Matrix dimensions
# This vector contains the dimensios of the matrices used in the executions
MDIM=(10 25 50 75 100 150 250 350 500 650 800 1000 1250 1500 2000 2500 3000 3500
      4000 4500 5000 5500 6000 6500 7000 7500 8000)

# Number of repetitions for each function call. The final performance will be
# computed as the average of all the repetitions
NREP=2

# Position (0-based) in the vector MDIM from which (included) onwards the matrix
# dimensions will be used for computing the scalability. Small dimensions should
# be avoided in this computation. This variable is used by the script 'figures'
PMDIM=14

# Figure format for the final plots: 'pdf', 'png', 'jpg', etc.
FORMAT="png"

# Colours for plotting. This vector must contain more or equal elements than
# PROGRAMS
COLOURS=("b" "r" "g" "m" "k" "c" "b" "r" "g" "m" "k" "c" "b" "r" "g" "m" "k")

# Marks for plotting. This vector must contain more or equal elements than
# PROGRAMS
MARKS=("s" "^" "o" "*" "D" "v" "p" "x" "s" "^" "o" "*" "D" "v" "p" "x" "s")

# Position of the legend. It can be: "best", "upper right", "upper left",
# "lower left", "lower right", "right", "center left", "center right",
# "lower center", "upper center", "center"
POSLEGEND="best"

# Use of pdfcrop for crop the possible PDF generated files: 0/1 -> no/yes
# The matplotlib command 'savefig' tries to adjust the plots limits to the real
# figure, but for PDF the 'pdfcrop' program gets better results
CROPPDF=1

# pdfcrop executable
PATHPDFCROP=pdfcrop

# Location of the Python executable
PATHPY=/usr/bin/python

# Base directory. By default, the current directory
PATHBASE=`pwd`

# Name of the parameters file
PATHPARFILE=$PATHBASE/parameters

# Path where the programs are
PATHPROGS=$PATHBASE/programs

# Path to store the crude results
PATHNUMBERS=$PATHBASE/numbers

# Path to store the plots
PATHPLOTS=$PATHBASE/plots
################################################################################
function IsBLAS()
{
    # Convert the input argument to upper case
    FUNNAME=`echo $1 | tr '[:lower:]' '[:upper:]'`
    # Check if it is a BLAS function
    if [ "$FUNNAME" = "SGEMM" ] || [ "$FUNNAME" = "DGEMM" ] ||
       [ "$FUNNAME" = "STRMM" ] || [ "$FUNNAME" = "DTRMM" ] ||
       [ "$FUNNAME" = "SSYRK" ] || [ "$FUNNAME" = "DSYRK" ]; then
        ISBLAS=1
    else
        ISBLAS=0
    fi
}
################################################################################
function IsLapack()
{
    # Convert the input argument to upper case
    FUNNAME=`echo $1 | tr '[:lower:]' '[:upper:]'`
    # Check if it is a Lapack function
    if [ "$FUNNAME" = "SGETRF" ] || [ "$FUNNAME" = "DGETRF" ] ||
       [ "$FUNNAME" = "SGEQRF" ] || [ "$FUNNAME" = "DGEQRF" ]; then
        ISLAPACK=1
    else
        ISLAPACK=0
    fi
}
