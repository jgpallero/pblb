.. -*- coding: utf-8 -*-
.. Written by José Luis García Pallero, jgpallero@gmail.com, September 2013
.. Public domain

********************************************************************************
``pblb``: Plots of BLAS and Lapack Benchmarks
********************************************************************************

Introduction
------------

This software provides an easy way to perform simple benchmarks of some BLAS
and/or Lapack functions (only for real ``float`` and ``double`` type of data).

Usage
-----

- Create the benchmark programs compiling the file ``programs/benchmark.c``.
  Note that this program call the Fortran version of BLAS and Lapack. For the
  compilation, the provided ``Makefile`` in the top directory can be used. The
  Makefile admits several options:

  - ``CC``: Sets the compiler. By default, ``gcc``.
  - ``CFLAGS``: Sets the compiler options. By default, ``-O2``.
  - ``LINK``: Sets the libraries for linking the executable. By default,
    ``-lrt -lblas -llapack``. Only if the program is compiled with Lapack
    support the ``-llapack`` is added.
  - ``LAP``: Sets if the support for Lapack function is compiled. By default,
    ``-DLAPACK``. If Lapack is not desired, set this flag empty as ``LAP=``.
  - ``DIR``: Sets the base directory for construct the input and output files.
    By default, ``./programs/``.
  - ``SRC``: Sets the name of the source code of the benchmark program. By
    default, ``$(DIR)/benchmark.c``.
  - ``OUT``: Sets the name of the output (executable) file. By default,
    ``testing``. The complete output file is created internally in other
    variable as ``RESULT=$(DIR)/$(OUT)``.

- Configure the parameters script ``configuration`` with the desired values. The
  different options are commented in the script itself.

- Run the Bash script ``compute``. This will perform the computations and will
  generate the performance files in the directory ``numbers/``.

- Run the script ``figures``. This will generate, based on the ``configuration``
  script and the content of ``numbers/``, the the Python scripts ``perfplot``
  and ``scalplot``, which will generate the final figures.

- Run the scripts ``perfplot`` and ``scalplot`` to generate the figures, which
  will be stored in the folder ``plots/``. The ``matplotlib`` and ``numpy``
  Python packages are required.

Additional notes
----------------

The ``benchmark.c`` program works only with square matrices. Also, the
parameters related with the other matrix characteristics for the different
functions are: ``Trans="N"``, ``Side="L"``, ``Uplo="U"`` and ``Diag="U"``. For
more elaborated benchmarks you should modify the source code of ``benchmark.c``.
