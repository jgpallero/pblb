# -*- coding: utf-8 -*-
# Written by José Luis García Pallero, jgpallero@gmail.com, September 2013
# Public domain

DIR=./programs/
SRC=$(DIR)/benchmark.c
OUT=testing
RESULT=$(DIR)/$(OUT)
CC=gcc
CFLAGS=-O2
LAP=-DLAPACK
LINK=-lrt -lblas
ifeq ($(LAP),-DLAPACK)
    LINK+=-llapack
endif
all:
	$(CC) $(CFLAGS) $(LAP) $(SRC) -o $(RESULT) $(LINK)
#-------------------------------------------------------------------------------
# kate: space-indent off; tab-indents on; indent-width 4; tab-width 4;
