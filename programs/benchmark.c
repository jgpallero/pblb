/* -*- coding: utf-8 -*- */
/* Written by José Luis García Pallero, jgpallero@gmail.com, September 2013 */
/* Public domain */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define LDFNAME 500
#define LLINE   500
#define LFNAME  10
#define BUFF    10
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// BLAS GEMM routines
void sgemm_(char* TransA,char* TransB,int* M,int* N,int* K,float* alpha,
            float* A,int* lda,float* B,int* ldb,float* beta,float* C,int* ldc,
            int lTransA,int lTransB);
void dgemm_(char* TransA,char* TransB,int* M,int* N,int* K,double* alpha,
            double* A,int* lda,double* B,int* ldb,double* beta,double* C,
            int* ldc,int lTransA,int lTransB);
// BLAS TRMM routines
void strmm_(char* Side,char* Uplo,char* Trans,char* Diag,int* M,int* N,
            float* alpha,float* A,int* lda,float* B,int* ldb,int lSide,
            int lUplo,int lTrans,int lDiag);
void dtrmm_(char* Side,char* Uplo,char* Trans,char* Diag,int* M,int* N,
            double* alpha,double* A,int* lda,double* B,int* ldb,int lSide,
            int lUplo,int lTrans,int lDiag);
// BLAS SYRK routines
void ssyrk_(char* Uplo,char* Trans,int* N,int* K,float* alpha,float* A,int* lda,
            float* beta,float* B,int* ldb,int lUplo,int lTrans);
void dsyrk_(char* Uplo,char* Trans,int* N,int* K,double* alpha,double* A,
            int* lda,double* beta,double* B,int* ldb,int lUplo,int lTrans);
// Lapack GETRF routines
void sgetrf_(int* M,int* N,float* A,int* lda,int* ipiv,int* info);
void dgetrf_(int* M,int* N,double* A,int* lda,int* ipiv,int* info);
// Lapack GEQRF routines
void sgeqrf_(int* M,int* N,float* A,int* lda,float* tau,float* work,int* lwork,
             int* info);
void dgeqrf_(int* M,int* N,double* A,int* lda,double* tau,double* work,
             int* lwork,int* info);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int* InputData(int argc,char* argv[],char fname[],int* nRep,int* nDim);
void* MatrixAlloc(int dim1,int dim2,size_t sizeElem);
void* VectorAlloc(int dim,size_t sizeElem);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int main(int argc,char* argv[])
{
    //loop indices
    int i=0,j=0;
    //function name
    char fname[LFNAME+1];
    //number of repetitions and dimensions
    int nRep=0,nDim=0,dimW=0;
    //data size
    size_t sizeElem=0;
    //dimensions
    int* dim=NULL;
    //working matrices and vectors
    void* matA=NULL;
    void* matB=NULL;
    void* matC=NULL;
    void* ipiv=NULL;
    void* vecTau=NULL;
    void* vecWork=NULL;
    int* vecAux=NULL;
    //time variables
    double t=0.0,t1=0.0,t2=0.0;
    struct timespec tcomp;
    //number of operations
    double nops=0.0;
    //auxiliary variables
    float fWorkAux[1];
    double dWorkAux[1];
    int info=0,lwork=0,lworkAux1=0,lworkAux2=0;
    float fscalar=1.1;
    double dscalar=1.1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //read input data
    dim = InputData(argc,argv,fname,&nRep,&nDim);
    //memory allocation
    if((!strcmp(fname,"SGEMM"))||(!strcmp(fname,"DGEMM")))
    {
        //data size
        sizeElem = (!strcmp(fname,"SGEMM")) ? sizeof(float) : sizeof(double);
        matA = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
        matB = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
        matC = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
    }
    else if((!strcmp(fname,"STRMM"))||(!strcmp(fname,"DTRMM"))||
            (!strcmp(fname,"SSYRK"))||(!strcmp(fname,"DSYRK")))
    {
        //data size
        sizeElem = ((!strcmp(fname,"STRMM"))||
                    (!strcmp(fname,"SSYRK"))) ? sizeof(float) : sizeof(double);
        matA = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
        matB = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
    }
#if defined(LAPACK)
    else if((!strcmp(fname,"SGETRF"))||(!strcmp(fname,"DGETRF")))
    {
        //data size
        sizeElem = (!strcmp(fname,"SGETRF")) ? sizeof(float) : sizeof(double);
        matA = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
        ipiv = VectorAlloc(dim[nDim-1],sizeElem);
    }
    else if((!strcmp(fname,"SGEQRF"))||(!strcmp(fname,"DGEQRF")))
    {
        //data size
        sizeElem = (!strcmp(fname,"SGEQRF")) ? sizeof(float) : sizeof(double);
        matA = MatrixAlloc(dim[nDim-1],dim[nDim-1],sizeElem);
        vecTau = VectorAlloc(dim[nDim-1],sizeElem);
        vecAux = (int*)VectorAlloc(dim[nDim-1],sizeElem);
        //maximun value for lwork
        lwork = -1;
        for(i=0;i<nDim;i++)
        {
            //working dimensions
            dimW = dim[i];
            if(!strcmp(fname,"SGEQRF"))
            {
                sgeqrf_(&dimW,&dimW,(float*)matA,&dimW,(float*)vecTau,fWorkAux,
                        &lwork,&info);
                lworkAux1 = (int)fWorkAux[0];
            }
            else
            {
                dgeqrf_(&dimW,&dimW,(double*)matA,&dimW,(double*)vecTau,
                        dWorkAux,&lwork,&info);
                lworkAux1 = (int)dWorkAux[0];
            }
            if(lworkAux1>lworkAux2)
            {
                lworkAux2 = lworkAux1;
            }
            vecAux[i] = lworkAux1;
        }
        vecWork = VectorAlloc(lworkAux2,sizeElem);
    }
#endif
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //loop over dimensions
    for(i=0;i<nDim;i++)
    {
        //working dimensions
        dimW = dim[i];
        //time variable
        t = 0.0;
        //number of repetitions
        for(j=1;j<=nRep;j++)
        {
            //starting time
            clock_gettime(CLOCK_REALTIME,&tcomp);
            t1 = (double)tcomp.tv_sec+(double)tcomp.tv_nsec*1.0e-9;
            //choose the correct function
            if(!strcmp(fname,"SGEMM"))
            {
                //number of operations
                nops = 2.0*dimW*dimW*dimW;
                //compute
                sgemm_("N","N",&dimW,&dimW,&dimW,&fscalar,(float*)matA,&dimW,
                       (float*)matB,&dimW,&fscalar,(float*)matC,&dimW,1,1);
            }
            else if(!strcmp(fname,"DGEMM"))
            {
                //number of operations
                nops = 2.0*dimW*dimW*dimW;
                //compute
                dgemm_("N","N",&dimW,&dimW,&dimW,&dscalar,(double*)matA,&dimW,
                       (double*)matB,&dimW,&dscalar,(double*)matC,&dimW,1,1);
            }
            else if(!strcmp(fname,"STRMM"))
            {
                //number of operations
                nops = ((double)dimW)*dimW*dimW;
                //compute
                strmm_("L","U","N","U",&dimW,&dimW,&fscalar,(float*)matA,&dimW,
                       (float*)matB,&dimW,1,1,1,1);
            }
            else if(!strcmp(fname,"DTRMM"))
            {
                //number of operations
                nops = ((double)dimW)*dimW*dimW;
                //compute
                dtrmm_("L","U","N","U",&dimW,&dimW,&dscalar,(double*)matA,&dimW,
                       (double*)matB,&dimW,1,1,1,1);
            }
            else if(!strcmp(fname,"SSYRK"))
            {
                //number of operations
                nops = (dimW+1.0)*dimW*dimW;
                //compute
                ssyrk_("U","N",&dimW,&dimW,&fscalar,(float*)matA,&dimW,&fscalar,
                       (float*)matB,&dimW,1,1);
            }
            else if(!strcmp(fname,"DSYRK"))
            {
                //number of operations
                nops = (dimW+1.0)*dimW*dimW;
                //compute
                dsyrk_("U","N",&dimW,&dimW,&dscalar,(double*)matA,&dimW,
                       &dscalar,(double*)matB,&dimW,1,1);
            }
#if defined(LAPACK)
            else if(!strcmp(fname,"SGETRF"))
            {
                //number of operations
                nops = 2.0/3.0*dimW*dimW*dimW-1.0/2.0*dimW*dimW+5.0/6.0*dimW;
                //compute
                sgetrf_(&dimW,&dimW,(float*)matA,&dimW,(int*)ipiv,&info);
                //check for errors
                if(info)
                {
                    fprintf(stderr,"Error in SGETRF\n");
                    exit(EXIT_FAILURE);
                }
            }
            else if(!strcmp(fname,"DGETRF"))
            {
                //number of operations
                nops = 2.0/3.0*dimW*dimW*dimW-1.0/2.0*dimW*dimW+5.0/6.0*dimW;
                //compute
                dgetrf_(&dimW,&dimW,(double*)matA,&dimW,(int*)ipiv,&info);
                //check for errors
                if(info)
                {
                    fprintf(stderr,"Error in DGETRF\n");
                    exit(EXIT_FAILURE);
                }
            }
            else if(!strcmp(fname,"SGEQRF"))
            {
                //number of operations
                nops = 4.0/3.0*dimW*dimW*dimW+2.0*dimW*dimW+14.0/3.0*dimW;
                //compute
                sgeqrf_(&dimW,&dimW,(float*)matA,&dimW,(float*)vecTau,
                        (float*)vecWork,&vecAux[i],&info);
                //check for errors
                if(info)
                {
                    fprintf(stderr,"Error in SGEQRF\n");
                    exit(EXIT_FAILURE);
                }
            }
            else if(!strcmp(fname,"DGEQRF"))
            {
                //number of operations
                nops = 4.0/3.0*dimW*dimW*dimW+2.0*dimW*dimW+14.0/3.0*dimW;
                //compute
                dgeqrf_(&dimW,&dimW,(double*)matA,&dimW,(double*)vecTau,
                        (double*)vecWork,&vecAux[i],&info);
                //check for errors
                if(info)
                {
                    fprintf(stderr,"Error in DGEQRF\n");
                    exit(EXIT_FAILURE);
                }
            }
#endif
            //finish time
            clock_gettime(CLOCK_REALTIME,&tcomp);
            t2 = (double)tcomp.tv_sec+(double)tcomp.tv_nsec*1.0e-9;
            t += t2-t1;
        }
        //time spent
        t /= ((double)nRep);
        //results printing
        fprintf(stdout," %6d %8.3lf\n",dimW,nops/t/1.0e9);
        fflush(stdout);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //clean memory
    free(matA);
    free(matB);
    free(matC);
    free(vecTau);
    free(ipiv);
    free(vecWork);
    free(vecAux);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //program exit
    return 0;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int* InputData(int argc,char* argv[],char fname[],int* nRep,int* nDim)
{
    //loop index
    int i=0;
    //datafile pointer
    FILE* file=NULL;
    //datafile name
    char dfname[LDFNAME+1];
    //auxiliary variables
    int dimAux=0,nDimAux=0;
    char line[LLINE+1];
    //dimensions
    int* dim=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //datafile name extraction
    if(argc==2)
    {
        strcpy(dfname,argv[1]);
    }
    else
    {
        fprintf(stderr,"There is an incorrect number of arguments\n");
        fprintf(stderr,"Try %s datafile\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //open the data file
    file = fopen(dfname,"rb");
    if(file==NULL)
    {
        fprintf(stderr,"Error opening '%s' file\n",dfname);
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //read the name of the function to use
    fgets(line,LLINE+1,file);
    if(sscanf(line,"%s",fname)!=1)
    {
        fprintf(stderr,"Error reading '%s' file\n",dfname);
        fprintf(stderr,"The name of the function to use could not be read\n");
        exit(EXIT_FAILURE);
    }
    //convert to upper case
    for(i=0;i<(int)strlen(fname);i++)
    {
        fname[i] = (char)toupper((int)fname[i]);
    }
    //check for Lapack
#if !defined(LAPACK)
    if((!strcmp(fname,"SGETRF"))||(!strcmp(fname,"DGETRF"))||
       (!strcmp(fname,"SGEQRF"))||(!strcmp(fname,"DGEQRF")))
    {
        fprintf(stderr,"Error reading '%s' file\n",dfname);
        fprintf(stderr,"The program has not Lapack capabilities\n");
        exit(EXIT_FAILURE);
    }
#endif
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //read the number of repetitions
    fgets(line,LLINE+1,file);
    if(sscanf(line,"%d",nRep)!=1)
    {
        fprintf(stderr,"Error reading '%s' file\n",dfname);
        fprintf(stderr,"The number of repetitions to use could not be read\n");
        exit(EXIT_FAILURE);
    }
    if((*nRep)<=0)
    {
        fprintf(stderr,"Error reading '%s' file\n",dfname);
        fprintf(stderr,"The number of repetitions is <= 0\n");
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    *nDim = 0;
    //read the matrix dimensions
    while(fgets(line,LLINE+1,file)!=NULL)
    {
        //read the dimension
        if(sscanf(line,"%d",&dimAux)!=1)
        {
            fprintf(stderr,"Error reading '%s' file\n",dfname);
            fprintf(stderr,"Empty line\n");
            exit(EXIT_FAILURE);
        }
        //check if the number is a positive integer
        if(dimAux<=0)
        {
            fprintf(stderr,"Error reading '%s' file\n",dfname);
            fprintf(stderr,"Matrix dimensions <= 0\n");
            exit(EXIT_FAILURE);
        }
        //check if more memory is needed
        if((*nDim)<=nDimAux)
        {
            nDimAux += BUFF;
            dim = (int*)realloc(dim,nDimAux*sizeof(int));
            if(dim==NULL)
            {
                fprintf(stderr,"Memory allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
        //assign the data
        dim[*nDim] = dimAux;
        (*nDim)++;
    }
    //check the number of matrices
    if(*nDim<=0)
    {
        fprintf(stderr,"Error reading '%s' file\n",dfname);
        fprintf(stderr,"No matrix dimensions where introduced\n");
        exit(EXIT_FAILURE);
    }
    //realloc the definitive memory
    if((*nDim)<nDimAux)
    {
        dim = (int*)realloc(dim,(*nDim)*sizeof(int));
        if(dim==NULL)
        {
            fprintf(stderr,"Memory allocation error\n");
            exit(EXIT_FAILURE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //close the data file
    fclose(file);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //function exit
    return dim;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void* MatrixAlloc(int dim1,int dim2,size_t sizeElem)
{
    //index for a loop
    int i=0;
    //auxiliary variable
    double aux=0.0;
    //auxiliary variables
    float* mfloat=NULL;
    double* mdouble=NULL;
    //output variable
    void* matrix=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //memory allocation
    matrix = malloc(dim1*dim2*sizeElem);
    if(matrix==NULL)
    {
        fprintf(stderr,"Matrix memory allocation error\n");
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //select the data type
    if(sizeElem==4)
    {
        mfloat = (float*)matrix;
    }
    else
    {
        mdouble = (double*)matrix;
    }
    //assign data to the matrix
    for(i=0;i<(dim1*dim2);i++)
    {
        //generate a random number between -1 and 1
        aux = -1.0+2.0*(((double)rand())/((double)RAND_MAX));
        //select the data type
        if(sizeElem==4)
        {
            mfloat[i] = (float)aux;
        }
        else
        {
            mdouble[i] = aux;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    return matrix;
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void* VectorAlloc(int dim,size_t sizeElem)
{
    //output variable
    void* vector=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //memory allocation
    vector = malloc(dim*sizeElem);
    if(vector==NULL)
    {
        fprintf(stderr,"Vector memory allocation error\n");
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    return vector;
}
